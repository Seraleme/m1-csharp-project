﻿using System;
using System.Collections.Generic;
using M1_Projet.deserialize;

namespace M1_Projet.services
{
    public class MathService : IMathService
    {
        public double ecartType(List<TimeSery> timeSeries)
        {
            return Math.Sqrt(variance(timeSeries));
        }

        public double ecartType(double variance)
        {
            return Math.Sqrt(variance);
        }

        public double variance(List<TimeSery> timeSeries)
        {
            double average = this.average(timeSeries);
            double numerator = 0;
            double denominator = 0;
            foreach (var timeSery in timeSeries)
            {
                if (timeSery.close != null)
                {
                    numerator += Math.Pow((timeSery.close ?? default(double)) - average, 2);
                    denominator++;
                }
            }

            return numerator / denominator;
        }

        private double average(List<TimeSery> timeSeries)
        {
            double somme = 0;
            foreach (var timeSery in timeSeries)
                somme += timeSery.close ?? default(double);
            return somme / timeSeries.Count;
        }
    }
}