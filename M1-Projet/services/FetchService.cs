﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using M1_Projet.deserialize;
using Newtonsoft.Json;

namespace M1_Projet.services
{
    public class FetchService : IFetchService
    {
        private readonly HttpClient client;

        public FetchService(HttpClient client)
        {
            this.client = client;
        }

        public async Task<T> fetchData<T>(params Object[] args) where T : IDeserialize, new()
        {
            string json = await client.GetStringAsync(new T().getURL(args));
            return JsonConvert.DeserializeObject<T>(json);
        }

        public BitmapImage fetchChartImage(string json)
        {
            return fetchImage($"https://quickchart.io/chart?c={json}");
        }

        public BitmapImage fetchImage(string url)
        {
            var fullFilePath = @url;
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(fullFilePath, UriKind.Absolute);
            bitmap.EndInit();
            return bitmap;
        }
    }
}