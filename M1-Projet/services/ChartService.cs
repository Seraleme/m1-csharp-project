﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using M1_Projet.deserialize;

namespace M1_Projet.services
{
    public class ChartService : IChartService
    {
        public string generateJsonChartLine(DeserializeAssets dataAssets)
        {
            using (StreamReader r = new StreamReader("assets/chartLine.json"))
            {
                string json = r.ReadToEnd();
                json = replaceInJson(json, dataAssets.data[0].timeSeries, "close", "Average");
                json = replaceInJson(json, dataAssets.data[0].timeSeries, "low", "Low");
                json = replaceInJson(json, dataAssets.data[0].timeSeries, "high", "High");
                return json;
            }
        }

        private string replaceInJson(string json, List<TimeSery> timeSeries, string field, string dataName)
        {
            StringBuilder stringBuilder = new StringBuilder("\"data\": [");
            foreach (TimeSery timeSery in timeSeries)
            {
                PropertyInfo propertyInfo = timeSery.GetType().GetProperty(field);
                if (propertyInfo != null)
                {
                    var value = propertyInfo.GetValue(timeSery, null);
                    if (value != null)
                    {
                        string s = value.ToString();
                        if (s != null)
                        {
                            stringBuilder.Append("{\"x\": \"");
                            stringBuilder.Append(UnixTimeToDateTime(timeSery.time).ToString("dd/MM/yyyy HH:mm"));
                            stringBuilder.Append("\",\"y\":");
                            stringBuilder.Append(s.Replace(",", "."));
                            stringBuilder.Append("},");
                        }
                    }
                }
            }

            stringBuilder.Length--;
            stringBuilder.Append("]");
            json = json.Replace("\"data\": [\"" + dataName + "\"]", stringBuilder.ToString());
            return json;
        }

        private DateTime UnixTimeToDateTime(long unixtime)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
                .AddSeconds(unixtime)
                .ToLocalTime();
        }
    }
}