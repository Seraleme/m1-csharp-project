﻿using M1_Projet.deserialize;

namespace M1_Projet.services
{
    public interface IChartService
    {
        string generateJsonChartLine(DeserializeAssets dataAssets);
    }
}