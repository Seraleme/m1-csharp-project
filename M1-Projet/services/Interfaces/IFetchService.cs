﻿using System;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using M1_Projet.deserialize;

namespace M1_Projet.services
{
    public interface IFetchService
    {
        Task<T> fetchData<T>(params Object[] args) where T : IDeserialize, new();
        BitmapImage fetchChartImage(string json);
        BitmapImage fetchImage(string url);
    }
}