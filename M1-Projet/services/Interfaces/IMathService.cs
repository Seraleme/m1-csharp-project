﻿using System.Collections.Generic;
using M1_Projet.deserialize;

namespace M1_Projet.services
{
    public interface IMathService
    {
        double ecartType(List<TimeSery> timeSeries);
        double ecartType(double variance);
        double variance(List<TimeSery> timeSeries);
    }
}