﻿using System;
using System.Windows;
using System.Windows.Threading;
using M1_Projet.deserialize;
using M1_Projet.Injection;
using M1_Projet.services;
using M1_Projet.views;
using Ninject;

namespace M1_Projet
{
    public partial class MainWindow : Window
    {
        private readonly IMathService _mathService;
        private readonly IFetchService _fetchService;
        private readonly IChartService _chartService;

        private PageDetail _pageDetail;
        private PageDashboard _pageDashboard;

        private DeserializeMeta dataMeta;

        public MainWindow()
        {
            InitializeComponent();
            var kernel = new StandardKernel(new CCInjectionModule());
            _fetchService = kernel.Get<IFetchService>();
            _mathService = kernel.Get<IMathService>();
            _chartService = kernel.Get<IChartService>();

            PagesService.Content = new PageLoading();
            InitializeWindow();

            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += OnTimedEvent;
            dispatcherTimer.Interval = new TimeSpan(1, 0, 0);
            dispatcherTimer.Start();
        }

        private void OnTimedEvent(object sender, EventArgs e)
        {
            _pageDashboard.Refresh();
            _pageDetail.Refresh();
        }

        public async void InitializeWindow()
        {
            try
            {
                dataMeta = await _fetchService.fetchData<DeserializeMeta>();
                if (dataMeta == null || dataMeta.data.Count == 0)
                {
                    MessageBox.Show("Une erreur c'est produite lors de la récupération des données");
                    return;
                }

                _pageDetail = new PageDetail(dataMeta, _mathService, _fetchService, _chartService);
                _pageDashboard = new PageDashboard(dataMeta, _fetchService);
                PagesService.Content = _pageDetail;
            }
            catch (Exception e)
            {
                if (MessageBox.Show("Réessayer de charger les données ?", "Question", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    InitializeWindow();

                Console.WriteLine(e);
            }
        }

        private void OnClickPage1(object sender, RoutedEventArgs e)
        {
            if (_pageDetail == null)
                _pageDetail = new PageDetail(dataMeta, _mathService, _fetchService, _chartService);
            PagesService.Content = _pageDetail;
        }

        private void OnClickPage2(object sender, RoutedEventArgs e)
        {
            if (_pageDashboard == null)
                _pageDashboard = new PageDashboard(dataMeta, _fetchService);
            PagesService.Content = _pageDashboard;
        }
    }
}