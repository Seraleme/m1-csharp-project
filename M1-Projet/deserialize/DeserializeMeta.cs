﻿using System;
using System.Collections.Generic;

namespace M1_Projet.deserialize
{
#pragma warning disable 8632
    public class DeserializeMeta : IDeserialize
    {
        public List<DatumMeta> data { get; set; }

        public string getURL(params Object[] args)
        {
            return $"https://api.lunarcrush.com/v2?data=meta&key={IDeserialize.KEY}&type=price";
        }
    }

    public class DatumMeta
    {
        public string name { get; set; }
        public string symbol { get; set; }
    }
}