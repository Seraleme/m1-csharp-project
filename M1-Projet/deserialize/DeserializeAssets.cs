﻿using System;
using System.Collections.Generic;

namespace M1_Projet.deserialize
{
    // DeserializeAssets myDeserializedClass = JsonConvert.DeserializeObject<DeserializeAssets>(myJsonResponse); 
    public class DeserializeAssets : IDeserialize
    {
#pragma warning disable 8632
        public List<Datum> data { get; set; }

        public string getURL(params Object[] args)
        {
            return String.Format("https://api.lunarcrush.com/v2?data=assets&key=" + IDeserialize.KEY + "&symbol={0}",
                args);
        }
    }


    public class TimeSery
    {
        public long time { get; set; }
        public double? close { get; set; }
        public double? high { get; set; }
        public double? low { get; set; }
    }

    public class Datum
    {
        public string name { get; set; }
        public double price { get; set; }
        public double price_btc { get; set; }
        public long market_cap { get; set; }
        public double percent_change_7d { get; set; }
        public double volume_24h { get; set; }
        public double? max_supply { get; set; }
        public List<TimeSery> timeSeries { get; set; }
        public double tweet_retweets { get; set; }
    }
}