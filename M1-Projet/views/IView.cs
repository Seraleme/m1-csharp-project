﻿namespace M1_Projet.views
{
    public interface IView
    {
        void Refresh();
    }
}