﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using M1_Projet.deserialize;
using M1_Projet.services;

namespace M1_Projet.views
{
    public partial class PageDetail : Page, IView
    {
        private readonly IMathService _mathService;
        private readonly IFetchService _fetchService;
        private readonly IChartService _chartService;


        public PageDetail(DeserializeMeta dataMeta, IMathService mathService, IFetchService fetchService,
            IChartService chartService)
        {
            _mathService = mathService;
            _fetchService = fetchService;
            _chartService = chartService;
            InitializeComponent();
            InitializeView(dataMeta);
        }

        public void InitializeView(DeserializeMeta dataMeta)
        {
            for (var i = 0; i < dataMeta.data.Count && i < 100; i++)
                comboBox.Items.Add(new ComboBoxItem {Content = dataMeta.data[i].name, Uid = dataMeta.data[i].symbol});
        }

        public async void SetViewContent(string moneySymbol)
        {
            try
            {
                DeserializeAssets dataAssets =
                    await _fetchService.fetchData<DeserializeAssets>(moneySymbol);

                if (dataAssets == null)
                {
                    MessageBox.Show("Une erreur c'est produite lors de la récupération des données");
                    return;
                }

                titleBox.Text = dataAssets.data[0].name;

                priceBox.Text = String.Format("${0:n}", dataAssets.data[0].price);
                priceBTCBox.Text = String.Format("{0:n}BTC", dataAssets.data[0].price_btc);
                circulatingSupplyBox.Text = String.Format("{0:n}M", dataAssets.data[0].max_supply / 1000000d);
                changeBox.Text = String.Format("{0:n}%", dataAssets.data[0].percent_change_7d);
                changeBox.Foreground =
                    dataAssets.data[0].percent_change_7d < 0 ? Brushes.Tomato : Brushes.MediumSeaGreen;

                marketCapBox.Text = String.Format("${0:n}T", dataAssets.data[0].market_cap / 1000000000000d);
                volumeBox.Text = String.Format("${0:n}B", dataAssets.data[0].volume_24h / 1000000000d);

                varianceBox.Text = _mathService.variance(dataAssets.data[0].timeSeries).ToString("0.##");
                ecartTypeBox.Text = _mathService.ecartType(dataAssets.data[0].timeSeries).ToString("$0.##");

                imageBox.Source = _fetchService.fetchChartImage(_chartService.generateJsonChartLine(dataAssets));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                Console.WriteLine(e);
            }
        }

        private void ComboBoxClick(object sender, SelectionChangedEventArgs e)
        {
            SetViewContent(((ComboBoxItem) comboBox.SelectedItem).Uid);
        }

        public void Refresh()
        {
            SetViewContent(((ComboBoxItem) comboBox.SelectedItem).Uid);
        }
    }
}