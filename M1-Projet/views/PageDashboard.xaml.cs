﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using M1_Projet.deserialize;
using M1_Projet.services;

namespace M1_Projet.views
{
    public partial class PageDashboard : Page, IView
    {
        private readonly IFetchService _fetchService;

        private DeserializeMeta dataMeta;

        public PageDashboard(DeserializeMeta dataMeta, IFetchService fetchService)
        {
            InitializeComponent();
            this.dataMeta = dataMeta;
            _fetchService = fetchService;
            InitializeView();
        }


        public void InitializeView()
        {
            panel.Children.Clear();
            for (var i = 0; i < dataMeta.data.Count && i < 100; i++)
            {
                StackPanel line = new StackPanel();
                line.Orientation = Orientation.Horizontal;

                Image image = new Image();
                image.Source = _fetchService.fetchImage(
                    $"https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/32x32/{dataMeta.data[i].name?.ToLower()}.png");
                image.Width = 32;
                image.Height = 32;
                line.Children.Add(image);

                line.Children.Add(createTextBlock(dataMeta.data[i].name + " - " + dataMeta.data[i].symbol));

                fetchValue(dataMeta.data[i].symbol, line);

                panel.Children.Add(line);
            }
        }

        public async void fetchValue(string moneySymbol, StackPanel line)
        {
            try
            {
                DeserializeAssets dataAssets =
                    await _fetchService.fetchData<DeserializeAssets>(moneySymbol);

                if (dataAssets == null)
                {
                    MessageBox.Show("Une erreur c'est produite lors de la récupération des données");
                    return;
                }

                line.Children.Add(createTextBlock(String.Format("${0:n}", dataAssets.data[0].price)));
                line.Children.Add(
                    createTextBlock(formatMarketCap(dataAssets.data[0].market_cap)));
                TextBlock textBlock = createTextBlock(String.Format("{0:n}%", dataAssets.data[0].percent_change_7d));
                textBlock.Foreground =
                    dataAssets.data[0].percent_change_7d < 0 ? Brushes.Tomato : Brushes.MediumSeaGreen;
                line.Children.Add(textBlock);

                line.Children.Add(
                    createTextBlock(String.Format("{0:n}M", (dataAssets.data[0].max_supply / 1000000d ?? 0))));
                line.Children.Add(createTextBlock(String.Format("{0:n}", dataAssets.data[0].tweet_retweets)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public string formatMarketCap(long market_cap)
        {
            if (market_cap < 1000000000000d)
                return String.Format("${0:n}B", market_cap / 1000000000d);
            else
                return String.Format("${0:n}T", market_cap / 1000000000000d);
        }

        public TextBlock createTextBlock(string format, string value)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.Text = String.Format(format, value);
            textBlock.Foreground = Brushes.White;
            textBlock.Margin = new Thickness(0, 0, 0, 10);
            textBlock.TextAlignment = TextAlignment.Center;
            textBlock.Width = 195;
            textBlock.FontSize = 18;
            return textBlock;
        }

        public TextBlock createTextBlock(string value)
        {
            return createTextBlock("{0}", value);
        }

        public void Refresh()
        {
            InitializeView();
        }
    }
}