﻿using System.Net.Http;
using M1_Projet.services;
using Ninject.Modules;

namespace M1_Projet.Injection
{
    public class CCInjectionModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IChartService>().To<ChartService>();
            Bind<IFetchService>().To<FetchService>();
            Bind<IMathService>().To<MathService>();
            Bind<HttpClient>().To<HttpClient>();
        }
    }
}